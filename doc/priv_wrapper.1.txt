priv_wrapper(1)
===============
:revdate: 2022-09-05
:author: Samba Team

NAME
----

priv_wrapper - A library to disable resource limits and other priviledge dropping.

SYNOPSIS
--------

LD_PRELOAD=libpriv_wrapper.so PRIV_WRAPPER=1 [PRIV_WRAPPER_CHROOT_DISABLE=1] [PRIV_WRAPPER_PRCTL_DISABLE=<options>] [PRIV_WRAPPER_SETRLIMIT_DISABLE=<options>]

DESCRIPTION
-----------

priv_wrapper aims to help running processes which are dropping privileges or are
restricting resources in test environments.
It can disable chroot, prctl, pledge and setrlmit system calls. A disabled call always
succeeds (i.e. returns 0) and does nothing.
The system call pledge exists only on OpenBSD.

ENVIRONMENT VARIABLES
---------------------

*PRIV_WRAPPER*::

This variable activates priv_wrapper when set to '1'.

*PRIV_WRAPPER_DISABLE_DEEPBIND*::

This allows you to disable deep binding in priv_wrapper.
This is useful for running valgrind tools or sanitizers like (address, undefined, thread).

*PRIV_WRAPPER_CHROOT_DISABLE*::

If this is set to '1' then chroot() system call will be disabled.

*PRIV_WRAPPER_PRCTL_DISABLE*::

prctl calls can be disabled using this environment variable. You can either disable all
calls using PRIV_WRAPPER_PRCTL_DISABLE=ALL or special options using e.g.
PRIV_WRAPPER_PRCTL_DISABLE=PR_SET_SECCOMP|PR_SET_NO_NEW_PRIVS

Supported options are:

PR_SET_SECCOMP
PR_SET_NO_NEW_PRIVS
PR_SET_DUMPABLE

*PRIV_WRAPPER_SETRLIMIT_DISABLE*::

Either all resource limits can be disabled using PRIV_WRAPPER_SETRLIMIT_DISABLE=ALL or
you can pick specific resources using e.g:

PRIV_WRAPPER_SETRLIMIT_DISABLE=RLIMIT_STACK|RLIMIT_CORE

Supported options are:

RLIMIT_CPU
RLIMIT_FSIZE
RLIMIT_DATA
RLIMIT_STACK
RLIMIT_CORE
RLIMIT_RSS
RLIMIT_NOFILE
RLIMIT_AS
RLIMIT_NPROC
RLIMIT_MEMLOCK
RLIMIT_LOCKS
RLIMIT_SIGPENDING
RLIMIT_MSGQUEUE
RLIMIT_NICE
RLIMIT_RTPRIO
RLIMIT_RTTIME
RLIMIT_NLIMITS

*PRIV_WRAPPER_PLEDGE_DISABLE*::

If this is set to '1' then pledge() system call will be disabled.

*PRIV_WRAPPER_DEBUGLEVEL*::

If you need to see what is going on in priv_wrapper itself or try to find a
bug, you can enable logging support in priv_wrapper if you built it with
debug symbols.

- 0 = ERROR
- 1 = WARNING
- 2 = DEBUG
- 3 = TRACE

EXAMPLE
-------

LD_PRELOAD=libpriv_wrapper.so PRIV_WRAPPER=1 PRIV_WRAPPER_PRCTL_DISABLE="PR_SET_SECCOMP|PR_SET_NO_NEW_PRIVS"

LD_PRELOAD=libpriv_wrapper.so PRIV_WRAPPER=1 PRIV_WRAPPER_CHROOT_DISABLE=1 PRIV_WRAPPER_PRCTL_DISABLE=ALL PRIV_WRAPPER_SETRLIMIT_DISABLE="RLIMIT_CPU|RLIMIT_DATA|RLIMIT_NLIMITS"
